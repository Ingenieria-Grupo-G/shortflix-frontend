import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    submitForm() {
      $.ajax({
        type: "POST",
        url: "http://localhost:8080/api/new-short",
        data: { title: $('.title-input').val(), cover: $('.cover-input').val(), video: $('.video-input').val(), 
                description: $('.description-input').val() }
      }).done(function(data) {
        if (data == 'success') {
          $('.bootstrap-alert').after('<div class="alert alert-success">El corto de creo correctamente</div>');
        }
      });
    },
  }
});
