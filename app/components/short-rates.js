import Ember from 'ember';

export default Ember.Component.extend({
	actions: {

	 votePositive() {
      	thumbsUp = parseInt(this.$("#thumbsUp").text())
      	thumbsUp += 1;
        this.$("#thumbsUp").text(thumbsUp);
    },
    voteNegative() {
		thumbsDown = parseInt(this.$("#thumbsDown").text())
      	thumbsDown -= 1;
        this.$("#thumbsDown").text(thumbsDown);
    }
  }

});
